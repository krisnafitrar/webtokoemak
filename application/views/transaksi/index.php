<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h3 class="mb-4 text-dark"><b><?= $title; ?></b></h3>
        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="<?= base_url('transaksi'); ?>" method="post">
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input type="text" id="keyword" name="keyword" class="form-control" placeholder="Cari <?= $title; ?>" aria-label="Cari Data Barang ..." aria-describedby="button-addon4" autocomplete="off" autofocus>
                                <div class="input-group-append" id="button-addon4">
                                    <input class="btn btn-success" type="submit" name="submit">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>
                        <?= $this->session->flashdata('message'); ?>
                        <div class="mb-3">
                            <ul class="nav nav-pills mt-3">
                                <li class="nav-item">
                                    <a class="nav-link <?= $active == 'all' ? 'active' : '' ?>" href="<?= base_url('transaksi/index') ?>">Semua</a>
                                </li>
                                <?php foreach ($a_nav as $nav) : ?>
                                    <li class="nav-item">
                                        <a class="nav-link <?= $active == $nav['idstatus'] ? 'active' : '' ?>" href="<?= base_url('transaksi/index/' . $nav['idstatus']) ?>"><?= $nav['status'] ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <form method="post" id="form-list">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">Kode Trx</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Waktu</th>
                                        <th scope="col">Grand Total</th>
                                        <th scope="col">Toko</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($transaksi as $s) : ?>
                                        <tr>
                                            <td><?= $s['kodetransaksi']; ?></td>
                                            <?php if ($s['is_seen'] == 1) : ?>
                                                <td><?= $s['nama']; ?><span class="badge badge-danger ml-3">New</span></td>
                                            <?php else : ?>
                                                <td><?= $s['nama']; ?></td>
                                            <?php endif; ?>
                                            <td><?= $s['waktu']; ?></td>
                                            <td><?= toRupiah($s['total']); ?></td>
                                            <td><?= $s['namausaha']; ?></td>
                                            <td><span class="badge badge-<?= $s['color'] ?>"><?= $s['status']; ?></span></td>
                                            <td>
                                                <button type="button" data-type="detail" data-id="<?= rawurlencode($s['kodetransaksi']); ?>" class="btn btn-sm btn-info btn-block">Detail</button>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                    <?php if (count($transaksi) < 1) : ?>
                                        <tr>
                                            <td colspan="7" align="center" class="text-danger">Transaksi tidak ditemukan</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script>
    $('[data-type=detail]').click(function() {
        var id = $(this).attr('data-id');
        location.href = `<?= base_url('transaksi/detail/') ?>${id}`;
    });
</script>