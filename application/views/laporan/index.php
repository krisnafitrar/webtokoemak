<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <h3 class="text-dark mb-4"><b><?= $title; ?></b></h3>
                        <div class="mb-2">
                            <?= $this->session->flashdata('message') ?>
                        </div>
                        <form action="<?= base_url('laporan/report') ?>" method="POST">
                            <div class="row mb-3">
                                <div class="col-md-2 mt-2">
                                    <label for="">From date</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="date" name="from_date" id="from_date" class="form-control" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-2 mt-2">
                                    <label for="">To date</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="date" name="to_date" id="to_date" class="form-control" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-2 mt-2">
                                    <label for="report_type">Report type</label>
                                </div>
                                <div class="col-md-6">
                                    <select name="report_type" id="report_type" class="form-control">
                                        <option value=""> -- Pilih Report type -- </option>
                                        <?php foreach ($a_report as $repp => $val) : ?>
                                            <option value="<?= $repp ?>"><?= $val ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-2 mt-2">
                                    <label for="file_type">File type</label>
                                </div>
                                <div class="col-md-6">
                                    <select name="file_type" id="file_type" class="form-control">
                                        <option value=""> -- Pilih File type -- </option>
                                        <option value="pdf">PDF</option>
                                        <option value="excel">MS.Excel</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-2 mt-2">
                                    <label for="email_to">Email to</label>
                                </div>
                                <div class="col-md-6">
                                    <select name="email_to" id="email_to" class="form-control">
                                        <?php foreach ($a_send as $key => $val) : ?>
                                            <option value="<?= $key ?>"><?= $val ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3 ml-0 mt-2">
                                <button type="submit" class="btn btn-info"><i class="fa fa-print"></i> Show Report</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->