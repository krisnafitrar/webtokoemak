<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><b><?= $title; ?></b></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="<?= base_url('pembelian/checkout') ?>" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 mt-2">
                                        <label for="nofaktur"><strong>No Faktur</strong></label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="nofaktur" id="nofaktur" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 mt-2">
                                        <label for="tanggal"><strong>Tanggal</strong></label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="date" class="form-control" name="tanggal" id="tanggal" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="supplier"><strong>Supplier</strong></label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="supplier" id="supplier" class="form-control" required></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <hr>
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-sm btn-success" id="btn-simpan"><i class="fa fa-print mr-1"></i>Simpan</button>
                        <a href="<?= base_url('pembelian/destroyCart') ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash mr-1"></i> Kosongkan Keranjang</a>
                    </div>
                </form>
                <div class="mt-2">
                    <?= $this->session->flashdata('message') ?>
                </div>
                <table class="table">
                    <thead>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Harga Beli</th>
                        <th>Harga Jual</th>
                        <th>Qty</th>
                        <th>Subtotal</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        <form action="<?= base_url('pembelian') ?>" method="POST">
                            <tr>
                                <td colspan="2"><select name="idbarang" id="idbarang" class="form-control" required></select></td>
                                <td colspan="2"><input type="number" class="form-control" name="harga" id="harga" placeholder="Harga" required></td>
                                <td colspan="2"><input type="number" class="form-control" name="qty" id="qty" placeholder="Qty" required></td>
                                <td>
                                    <button type="submit" class="btn btn-success">Tambah</button>
                                </td>
                            </tr>
                        </form>
                        <?php $total = 0; ?>
                        <?php foreach ($this->cart->contents() as $val) : ?>
                            <tr>
                                <td><?= $val['id'] ?></td>
                                <td><?= $val['name'] ?></td>
                                <td><?= toRupiah($val['price']) ?></td>
                                <td><?= toRupiah($val['options']['harga_jual']) ?></td>
                                <td><?= $val['qty'] ?></td>
                                <td><?= toRupiah($val['subtotal']) ?></td>
                                <td>
                                    <button data-type="btn-update" data-id="<?= $val['rowid'] ?>" data-jumlah="<?= $val['qty'] ?>" data-harga="<?= $val['options']['harga_jual'] ?>" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                                    <a href="<?= base_url('pembelian/deleteCart/' . $val['rowid']) ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash-alt"></i></a>
                                </td>
                            </tr>
                            <?php $total += $val['subtotal'] ?>
                        <?php endforeach; ?>
                        <tr class="table table-primary">
                            <td colspan="5" align="right"><b>Total</b></td>
                            <td colspan="3"><?= toRupiah($total); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Update</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('pembelian/index') ?>" method="post" id="modal_post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="harjual">Harga Jual</label>
                        <input type="number" class="form-control" id="harjual" name="harjual" placeholder="Harga Jual" required>
                    </div>
                    <div class="form-group">
                        <label for="qty">Qty</label>
                        <input type="number" class="form-control" id="qty" name="qty" placeholder="Qty" required>
                    </div>
                </div>
                <input type="hidden" id="act" name="act">
                <input type="hidden" id="key" name="key">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" data-type="simpan" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    let cartCount = '<?= count($this->cart->contents()) ?>';
    let btnSimpan = $('#btn-simpan');

    if (cartCount > 0) {
        btnSimpan.show();
    } else {
        btnSimpan.hide();
    }


    $('#idbarang').select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'Masukkan nama barang',
        ajax: {
            dataType: 'json',
            type: 'POST',
            url: '<?= site_url('pembelian/getbarang/') ?>',
            delay: 250,
            data: function(params) {
                return {
                    cari: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: data
                };
            },
        }
    });

    $('#supplier').select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'Masukkan nama supplier',
        ajax: {
            dataType: 'json',
            type: 'POST',
            url: '<?= site_url('pembelian/getSupplier/') ?>',
            delay: 250,
            data: function(params) {
                return {
                    cari: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: data
                };
            },
        }
    });

    $('[data-type=btn-update]').click(function() {
        let id = $(this).attr('data-id');
        let harga = $(this).attr('data-harga');
        let qty = $(this).attr('data-jumlah');
        const modal = $('#newMenuModal');
        modal.find('#harjual').val(harga);
        modal.find('#qty').val(qty);
        modal.find('#key').val(id);
        modal.find('#act').val('update');
        modal.modal();
    });
</script>