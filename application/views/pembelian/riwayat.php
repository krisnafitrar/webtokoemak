<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h3 class="mb-4 text-dark"><b><?= $title; ?></b></h3>
        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="<?= base_url('pembelian/riwayat'); ?>" method="post">
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input type="text" id="keyword" name="keyword" class="form-control" placeholder="Cari <?= $title; ?>" aria-label="Cari Data Barang ..." aria-describedby="button-addon4" autocomplete="off" autofocus>
                                <div class="input-group-append" id="button-addon4">
                                    <input class="btn btn-success" type="submit" name="submit">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>
                        <?= $this->session->flashdata('message'); ?>
                        <form method="post" id="form-list">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">No Faktur</th>
                                        <th scope="col">Tgl Beli</th>
                                        <th scope="col">Supplier</th>
                                        <th scope="col">Grand Total</th>
                                        <th scope="col">Insert At</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($riwayat as $s) : ?>
                                        <tr>
                                            <td><?= $s['nofaktur']; ?></td>
                                            <td><?= $s['tanggal_beli']; ?></td>
                                            <td><?= $s['namasupplier']; ?></td>
                                            <td><?= toRupiah($s['total']); ?></td>
                                            <td><?= $s['insert_at'] ?></td>
                                            <td>
                                                <button type="button" data-type="detail" data-id="<?= rawurlencode($s['idpembelian']); ?>" class="btn btn-sm btn-info">Detail</button>
                                                <button type="button" data-type="cancel" data-id="<?= rawurlencode($s['idpembelian']); ?>" class="btn btn-sm btn-danger">Batalkan</button>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                    <?php if (count($riwayat) < 1) : ?>
                                        <tr>
                                            <td colspan="7" align="center" class="text-danger">Pembelian tidak ditemukan</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script>
    $('[data-type=detail]').click(function() {
        var id = $(this).attr('data-id');
        location.href = `<?= base_url('transaksi/detail/') ?>${id}`;
    });
</script>