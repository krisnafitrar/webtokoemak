<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h3 class="mb-4 text-dark"><b><?= $title; ?></b></h3>
        <div class="card shadow mb-4">
            <div class="card-body">
                <button type="button" class="btn btn-primary mb-3" data-type="tambah">Tambah Submenu</button>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>

                        <?= $this->session->flashdata('message'); ?>
                        <?= $this->session->flashdata('delete'); ?>
                        <?= $this->session->flashdata('submenu'); ?>
                        <?= $this->session->flashdata('submenu_delete'); ?>
                        <form action="" method="post" id="table-list">
                            <table class="table table-hover" id="data-table">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Menu</th>
                                        <th scope="col">Url</th>
                                        <!-- <th scope="col">Icon</th> -->
                                        <th scope="col">Active</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($submenu as $sm) : ?>
                                        <tr>
                                            <th scope="row"><?= $i; ?></th>
                                            <td><?= $sm['title']; ?></td>
                                            <td><?= $sm['menu']; ?></td>
                                            <td><?= $sm['url']; ?></td>
                                            <!-- <td><?= $sm['icon']; ?></td> -->
                                            <td align="center"><?= $sm['is_active']; ?></td>
                                            <td>
                                                <button data-type="edit" type="button" class="btn btn-sm btn-primary" data-id="<?= $sm['id']; ?>">Edit</button>
                                                <button data-type="btndelete" type="button" class="btn btn-sm btn-danger" data-id="<?= $sm['id']; ?>">Hapus</button>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Add New Submenu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('menu/submenu') ?>" method="post" id="modal_post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="title" name="title" placeholder="Submenu title" required>
                    </div>
                    <div class="form-group">
                        <select name="menu_id" id="menu_id" class="form-control" required>
                            <option value="">Select Menu</option>
                            <?php foreach ($menu as $m) : ?>
                                <option value="<?= $m['id']; ?>"><?= $m['menu']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="url" name="url" placeholder="Submenu url" required>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" name="is_active" id="is_active" checked>
                            <label class="form-check-label" for="is_active">
                                Active?
                            </label>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="act" name="act">
                <input type="hidden" id="key" name="key">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" data-type="simpan" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Submenu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menghapus Submenu?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" data-id="" data-type="delete" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('[data-type=btndelete]').click(function() {
        var id = $(this).attr('data-id');
        $('#modal-delete').find('[data-type=delete]').attr('data-id', id);
        $('#modal-delete').modal();
    });

    $('[data-type=delete]').click(function() {
        var id = $(this).attr('data-id');
        location.href = '<?= site_url('menu/deleteSubmenu/') ?>' + id;
    });

    $('[data-type=simpan]').click(function() {
        var act = $('#modal_post #act').val();
        var key = $('#modal_post #key').val();

        if (act == "") {
            $('#modal_post #act').val('simpan');
        }
        $('#modal_post').submit();
    });

    $('[data-type=tambah]').click(function() {
        var modal = $('#newMenuModal');
        $('#modal_post')[0].reset();
        modal.find('#newMenuModalLabel').html('Tambah Submenu');
        modal.modal();
    });

    $('[data-type=edit]').click(function() {
        var id = $(this).attr('data-id');
        Swal.showLoading();
        xhrfGetData("<?= site_url('menu/getSubmenu/') ?>" + id, function(data) {
            var modal = $('#newMenuModal');
            modal.find('#newMenuModalLabel').html('Edit Submenu');
            modal.find('#title').val(data.title);
            modal.find('#menu_id').val(data.menu_id);
            modal.find('#url').val(data.url);
            modal.find('#is_active').val(data.is_active);
            modal.find('#act').val('edit');
            modal.find('#key').val(data.id);
            Swal.close();
            modal.modal();
        });
    });


    $('#data-table').DataTable();
</script>