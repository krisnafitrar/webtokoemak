<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pembelian extends MY_Model
{
    protected $table = 'pembelian';
    protected $schema = '';
    public $key = 'idpembelian';
    public $value = 'nofaktur';

    function __construct()
    {
        parent::__construct();
    }

    public function getPembelian($id = null)
    {
        $cond = empty($id) ? "" : " WHERE p.idusaha=" . $id;
        $query = "SELECT * FROM pembelian p JOIN supplier s USING(idsupplier)" . $cond;
        return $this->db->query($query);
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getTable()
    {
        return $this->table;
    }
}
