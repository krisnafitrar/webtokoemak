<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_menu extends MY_Model
{
    protected $table = 'user_menu';
    protected $schema = '';
    public $key = 'id';
    public $value = 'menu';

    function __construct()
    {
        parent::__construct();
    }

    public function getSubMenu()
    {
        $query = "SELECT `user_sub_menu`.*, `user_menu` . `menu`
                  FROM `user_sub_menu` JOIN `user_menu`
                  ON `user_sub_menu`.`menu_id` = `user_menu`.`id`";

        return $this->db->query($query)->result_array();
    }

    public function getMenuByRole($idrole)
    {
        $this->load->model('M_submenu');

        $menu = $this->db->select('um.*')
            ->from($this->table . ' um')
            ->JOIN('user_access_menu uam', 'uam.menu_id = um.id')
            ->where(array('uam.role_id' => $idrole))
            ->get()
            ->result_array();

        foreach ($menu as $key => $val) {
            $sub = $this->M_submenu->getSubMenuByMenu($val['id']);
            foreach ($sub as $v) {
                $val['submenu'][] = $v;
            }

            $menu[$key] = $val;
        }

        return $menu;
    }
}
