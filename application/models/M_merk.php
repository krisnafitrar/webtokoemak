<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_merk extends MY_Model
{
    protected $table = 'merk';
    protected $schema = '';
    public $key = 'idmerk';
    public $value = 'merk';

    function __construct()
    {
        parent::__construct();
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getTable()
    {
        return $this->table;
    }
}
