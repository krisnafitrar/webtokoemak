<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_konten extends MY_Model
{
    protected $table = 'konten';
    protected $schema = '';
    public $key = 'idkonten';
    public $value = 'nama';

    function __construct()
    {
        parent::__construct();
    }
}
