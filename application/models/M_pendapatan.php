<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pendapatan extends MY_Model
{
    protected $table = 'pendapatan';
    protected $schema = '';
    public $key = 'idpendapatan';
    public $value = 'pemasukan';

    function __construct()
    {
        parent::__construct();
    }

    public function getPendapatan($from, $to)
    {
        return $this->db->query($this->query($from, $to));
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function query($from, $to)
    {
        return "SELECT DISTINCT(SUBSTR(a.`tanggal`, 1, 10)) AS tanggal,kodetransaksi,
        (
            SELECT 
                SUM(b.`$this->value`) 
            FROM 
                `$this->table` AS b 
            WHERE 
                SUBSTR(b.`tanggal`, 1, 10) = SUBSTR(a.`tanggal`, 1, 10) 
            LIMIT 1
        ) AS total 
    FROM 
        `$this->table` AS a 
    WHERE 
        SUBSTR(a.`tanggal`, 1, 10) >= '" . $from . "' 
        AND SUBSTR(a.`tanggal`, 1, 10) <= '" . $to . "' 
    ORDER BY 
        a.`tanggal` ASC";
    }
}
