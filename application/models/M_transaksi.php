<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_transaksi extends MY_Model
{
    protected $table = 'transaksi';
    protected $schema = '';
    public $key = 'kodetransaksi';
    public $value = '';

    function __construct()
    {
        parent::__construct();
    }

    public function getByStatus($status = null)
    {
        $where = empty($status) ? "" : " WHERE idstatus=$status";
        $query = "SELECT * FROM transaksi t JOIN users u ON t.iduser=u.id JOIN usaha USING(idusaha) JOIN statustransaksi USING(idstatus)" . $where;

        return $this->db->query($query);
    }

    public function getDetailTrans($id)
    {
        $query = "SELECT * FROM transaksi t JOIN users u ON t.iduser=u.id JOIN usaha USING(idusaha) JOIN statustransaksi USING(idstatus) JOIN transaksi_detail td USING(kodetransaksi) JOIN barang b ON td.idbarang=b.idbarang JOIN jenisbarang jb ON b.idjenis=jb.idjenis WHERE t.kodetransaksi='$id'";

        return $this->db->query($query);
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getTable()
    {
        return $this->table;
    }
}
