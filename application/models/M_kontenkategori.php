<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kontenkategori extends MY_Model
{
    protected $table = 'kontenkategori';
    protected $schema = '';
    public $key = 'idkontenkategori';
    public $value = 'kontenkategori';

    public function __construct()
    {
        parent::__construct();
    }
}
