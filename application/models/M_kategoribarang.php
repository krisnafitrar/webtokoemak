<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kategoribarang extends MY_Model
{
    protected $table = 'kategoribarang';
    protected $schema = '';
    public $key = 'idkategori,idbarang';

    function __construct()
    {
        parent::__construct();
    }

    function getRefBy($id)
    {
        $query = "SELECT * FROM kategoribarang JOIN kategori USING(idkategori) JOIN barang USING (idbarang) WHERE idkategori='$id'";
        return $this->db->query($query);
    }
}
