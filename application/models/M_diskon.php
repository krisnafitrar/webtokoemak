<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_diskon extends MY_Model
{
    protected $table = 'diskon';
    protected $schema = '';
    public $key = 'iddiskon';

    public function __construct()
    {
        parent::__construct();
    }

    public function getRef()
    {
        $query = "SELECT * FROM diskon JOIN barang USING(idbarang)";
        return $this->db->query($query)->result_array();
    }

    public function getRefBy($key)
    {
        $query = "SELECT * FROM diskon JOIN barang USING(idbarang) WHERE iddiskon='$key'";
        return $this->db->query($query);
    }
}
