<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_role extends MY_Model
{
    protected $table = 'user_role';
    protected $schema = '';
    public $key = 'id';
    public $value = 'role';

    function __construct()
    {
        parent::__construct();
    }
}
