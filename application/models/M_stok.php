<?php

class M_stok extends MY_Model
{
    protected $table = 'riwayat_stok';
    protected $schema = '';
    public $key = 'idriwayatstok';
    public $value = 'jumlahstok';

    function __construct()
    {
        parent::__construct();
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getTable()
    {
        return $this->table;
    }
}
