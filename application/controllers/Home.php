<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_user');
        $this->load->model('M_barang');
        $this->load->model('M_kategori');

        $this->user = $this->M_user->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Dashboard', site_url('home'));

        $data['title'] = 'Dashboard';
        $data['profile'] = 'My Profile';
        $data['user'] = $this->user;
        $data['barang'] = $this->M_barang->get()->num_rows();
        $data['kategori'] = $this->M_kategori->get()->num_rows();
        $data['users'] = $this->M_user->get()->num_rows();

        $this->template->load('template', 'home/beranda', $data);
    }
}
