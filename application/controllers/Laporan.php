<?php

use Mpdf\Mpdf;

class Laporan extends CI_Controller
{
    public $user;
    public $PDF;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_role', 'role');
        $this->load->model('M_user', 'user');
        $this->load->model('M_pendapatan', 'pendapatan');

        $this->PDF = new \Mpdf\Mpdf();

        $this->user = $this->user->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Laporan', '#');

        $a_report = [
            'inc' => 'Pendapatan',
            'ex' => 'Pengeluaran',
            'trx' => 'Transaksi',
            'buy' => 'Pembelian'
        ];

        $a_send = [
            'null' => 'Tidak dikirim',
            'ceo' => 'Chief Executive Officer',
            'manager' => 'Manager',
            'acc' => 'Akuntan',
            'all' => 'Semua pihak berwenang'
        ];

        $data['title'] = 'Laporan Tokoemak';
        $data['user'] = $this->user;
        $data['a_report'] = $a_report;
        $data['a_send'] = $a_send;
        $this->template->load('template', 'laporan/index', $data);
    }

    public function report()
    {
        if (!empty($_POST)) {
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');
            $report_type = $this->input->post('report_type');
            $file_type = $this->input->post('file_type');
            $email_to = $this->input->post('email_to');

            switch ($report_type) {
                case 'inc':
                    $this->_pendapatan($from_date, $to_date, $report_type, $file_type, $email_to);
                    break;
                default:
                    # code...
                    break;
            }
        }
    }

    private function _pendapatan($from, $to, $repp, $file, $email)
    {
        $data['repp_val'] = $this->pendapatan->getPendapatan($from, $to)->result_array();
        $data['from'] = $from;
        $data['to'] = $to;
        $view = $this->load->view('laporan/repp_pendapatan', $data, TRUE);
        $this->PDF->WriteHTML($view);
        $this->PDF->Output('report_pendapatan.pdf', 'D');

        if (!empty($email)) {
            switch ($email) {
                case 'ceo':
                    $email = $this->db->get_where('users', ['role_id' => 1])->row_array()['email'];
                    $ok = sendEmail('Laporan pendapatan tokoemak', $email, $view);
                    $ok ? setMessage('Berhasil mengirim email', 'success') : setMessage('Gagal mengirim email', 'danger');
                    redirect('laporan/index');
                    break;
                default:
                    # code...
                    break;
            }
        }
    }
}
