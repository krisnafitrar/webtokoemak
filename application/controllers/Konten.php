<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Konten extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_kontenkategori', 'kontenkategori');

        //set default
        $this->title = 'Konten Client';
        $this->menu = 'konten';
        $this->parent = '';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_data = $this->kontenkategori->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'nama', 'label' => 'Nama Konten'];
        $a_kolom[] = ['kolom' => 'idkontenkategori', 'label' => 'Kategori', 'type' => 'S', 'option' => $a_data];
        $a_kolom[] = ['kolom' => 'deskripsi', 'label' => 'Deskripsi', 'type' => 'A', 'is_null' => true, 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'url', 'label' => 'URL (Optional)', 'is_null' => true, 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'gambar', 'label' => 'Gambar', 'type' => 'F', 'file_type' => 'jpg|png|jpeg', 'path' => './assets/img/konten/', 'is_tampil' => false];

        $this->a_kolom = $a_kolom;
    }
}
