<?php

class Transaksi extends MY_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_transaksi', 'transaksi');
        $this->load->model('M_user', 'users');
        $this->user = $this->users->getBy(['username' => $this->session->userdata['username']])->row_array();

        setBadges($this->transaksi->getTable(), $this->transaksi->getKey());
    }

    public function index($id = null)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Transaksi On Going', '#');

        $data['title'] = 'Transaksi Konsumen';
        $data['user'] = $this->user;
        $data['a_nav'] = $this->db->get('statustransaksi')->result_array();
        $data['active'] = 'all';
        $data['transaksi'] = $this->transaksi->getByStatus()->result_array();
        if (!empty($id)) {
            $data['active'] = $id;
            $data['transaksi'] = $this->transaksi->getByStatus($id)->result_array();
        }
        $this->template->load('template', 'transaksi/index', $data);
    }

    public function detail($id)
    {
        //cegat dulu.. apakah id fake or real
        $x = $this->transaksi->getBy(['kodetransaksi' => $id])->num_rows();

        if ($x < 1) {
            setMessage('Transaksi tidak ditemukan', 'danger');
            redirect('transaksi');
        }
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Transaksi Konsumen', base_url('transaksi'));
        $this->breadcrumb->append_crumb('Detail Transaksi', '#');

        $data['title'] = 'Transaksi Konsumen';
        $data['sub_title'] = 'Detail Transaksi';
        $data['user'] = $this->user;
        $data['detail_trans'] = $this->transaksi->getDetailTrans($id)->result_array();
        $this->template->load('template', 'transaksi/detail', $data);
    }
}
