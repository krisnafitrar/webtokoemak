<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends MY_Controller
{
    public $user;

    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_kategori', 'kategori');
        $this->load->model('M_jenis', 'jenis');
        $this->load->model('M_merk', 'merk');
        $this->load->model('M_barang', 'barang');

        //load model
        $this->load->model('M_user', 'users');

        $this->user = $this->users->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index($id = null)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Masters', '#');
        $this->breadcrumb->append_crumb('Barang', '#');

        $data['title'] = 'Data Barang';
        $data['profile'] = 'Data Barang';
        $data['user'] = $this->user;
        $data['barang'] = $this->barang->getByKategori()->result_array();

        $this->template->load('template', 'masters/index', $data);
    }

    public function create()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Masters', '#');
        $this->breadcrumb->append_crumb('Data Barang', base_url('masters/barang'));
        $this->breadcrumb->append_crumb('Tambah Barang', '#');

        $data['title'] = 'Data Barang';
        $data['profile'] = 'Tambah Barang';
        $data['user'] = $this->user;

        $data['a_kolom'] = $this->_a_kolom();
        $this->template->load('template', 'masters/create', $data);
    }

    private function _a_kolom()
    {
        $a_kategori = $this->kategori->getListCombo();
        $a_jenis = $this->jenis->getListCombo();
        $a_merk = $this->merk->getListCombo();
        $a_data = [
            '1' => 'Aktif',
            '0' => 'Non-Aktif'
        ];

        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'idbarang', 'label' => 'ID Barang'];
        $a_kolom[] = ['kolom' => 'namabarang', 'label' => 'Nama barang'];
        $a_kolom[] = ['kolom' => 'idkategori', 'label' => 'Kategori', 'type' => 'S', 'option' => $a_kategori];
        $a_kolom[] = ['kolom' => 'idjenis', 'label' => 'Jenis', 'type' => 'S', 'option' => $a_jenis];
        $a_kolom[] = ['kolom' => 'idmerk', 'label' => 'Merk', 'type' => 'S', 'option' => $a_merk];
        $a_kolom[] = ['kolom' => 'harga_pokok', 'label' => 'Harga Pokok', 'type' => 'N', 'set_currency' => true];
        $a_kolom[] = ['kolom' => 'harga_jual', 'label' => 'Harga', 'type' => 'N', 'set_currency' => true];
        $a_kolom[] = ['kolom' => 'diskon', 'label' => 'Diskon'];
        $a_kolom[] = ['kolom' => 'stok', 'label' => 'Stok', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'deskripsi', 'label' => 'Deskripsi', 'type' => 'A', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'expired_at', 'label' => 'Expired', 'type' => 'D', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'gambar', 'label' => 'Gambar', 'type' => 'F', 'path' => './assets/img/produk/', 'file_type' => 'jpg|png', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'is_aktif', 'label' => 'Status', 'type' => 'S', 'option' => $a_data, 'is_tampil' => false];

        return $a_kolom;
    }

    public function edit($id)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Masters', '#');
        $this->breadcrumb->append_crumb('Data Barang', base_url('barang'));
        $this->breadcrumb->append_crumb('Edit Barang', '#');

        $data['title'] = 'Data Barang';
        $data['profile'] = 'Edit Barang';
        $data['user'] = $this->user;

        $data['a_kolom'] = $this->_a_kolom();
        $data['col_id'] = $id;
        $data['row'] = $this->barang->getById($id)->row_array();
        $this->template->load('template', 'masters/edit', $data);
    }

    public function data($id = null)
    {
        $a_record = array();
        if (!empty($_POST) || !empty($_FILES)) {
            foreach ($this->_a_kolom() as $col) {
                if (isset($_POST[$col['kolom']]) || $col['type'] == 'F' || $col['is_null']) {
                    if ($_FILES[$col['kolom']]['name'] != "") {
                        $a_record[$col['kolom']] = $_FILES[$col['kolom']];
                        $config['upload_path'] = $col['path'];
                        $config['allowed_types'] = $col['file_type'];
                        $config['max_size'] = empty($col['size']) ? 2048 : $col['size'];
                        $this->load->library('upload', $config);

                        if ($this->upload->do_upload($col['kolom'])) {
                            $file = base_url(substr($col['path'], 1)) . $this->upload->data('file_name');
                        } else {
                            setMessage($this->upload->display_errors(), 'danger');
                            empty($id) ? redirect('barang/create') : redirect('barang');
                        }
                    }
                    $a_record[$col['kolom']] = $_FILES[$col['kolom']] ? $file : ($col['type'] != 'P' ? ($col['type'] == 'N' ? intval($_POST[$col['kolom']]) : $_POST[$col['kolom']]) : password_hash($_POST[$col['kolom']], PASSWORD_DEFAULT));
                } else {
                    setMessage('Mohon lengkapi data', 'danger');
                    empty($id) ? redirect('barang/create') : redirect('barang');
                }
            }

            $ok = empty($id) ? $this->barang->insert($a_record) : $this->barang->update($a_record, $id);
            $message = empty($id) ? 'menambahkan barang baru' : 'merubah data barang';
            ($ok && $ok) ? setMessage('Berhasil ' . $message, 'success') : setMessage('Gagal ' . $message, 'danger');
            empty($id) ? redirect('barang/create') : redirect('barang');
        }
    }
}
