<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian extends MY_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //load model
        $this->load->model('M_user', 'users');
        $this->load->model('M_supplier', 'supplier');
        $this->load->model('M_barang', 'barang');
        $this->load->model('M_pembelian', 'pembelian');
        $this->load->model('M_pembeliandetail', 'belidetail');

        //load library
        $this->load->library('cart');

        $this->user = $this->users->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Pembelian Barang', '#');
        $data['title'] = 'Pembelian Barang';
        $data['profile'] = 'Pembelian';
        $data['user'] = $this->user;
        $this->template->load('template', 'pembelian/index', $data);

        if (!empty($_POST)) {
            $barang = $this->input->post('idbarang');
            $qty = $this->input->post('qty');
            $act = $this->input->post('act');
            $key = $this->input->post('key');
            $harga = $this->input->post('harga');
            $harga_jual = $this->input->post('harjual');

            $produk = getBarang()[$barang];

            if ($act == "update") {
                $data = [
                    'rowid' => $key,
                    'qty' => $qty,
                    'options' => [
                        'harga_jual' => $harga_jual
                    ]
                ];
                $this->cart->update($data);
            } else {
                $data = [
                    'id' => $barang,
                    'qty' => $qty,
                    'price' => $harga,
                    'name' => $produk['namabarang'],
                    'options' => [
                        'harga_jual' => $produk['harga_jual']
                    ]
                ];
                $this->cart->insert($data);
            }

            redirect('pembelian');
        }
    }

    public function getBarang()
    {
        $keyword = $this->input->post('cari');
        $query = "SELECT * FROM barang WHERE lower(namabarang) LIKE '%$keyword%' OR lower(idbarang) LIKE '%$keyword%'";
        $barang = $this->db->query($query);

        if ($barang->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($barang->result_array() as $val) {
                $data[] = array('id' => $val['idbarang'], 'text' => $val['idbarang'] . ' - ' . $val['namabarang']);
            }
            echo json_encode($data);
        }
    }

    public function getSupplier()
    {
        $keyword = $this->input->post('cari');
        $query = "SELECT * FROM supplier WHERE lower(namasupplier) LIKE '%$keyword%' OR lower(idsupplier) LIKE '%$keyword%'";
        $supplier = $this->db->query($query);

        if ($supplier->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($supplier->result_array() as $val) {
                $data[] = array('id' => $val['idsupplier'], 'text' => $val['idsupplier'] . ' - ' . $val['namasupplier']);
            }
            echo json_encode($data);
        }
    }

    public function destroyCart()
    {
        $ok = $this->cart->destroy();
        $ok ? setMessage('Berhasil membersihkan cart', 'success') : setMessage('Gagal membersihkan cart', 'danger');
        redirect('pembelian');
    }

    public function deleteCart($rowid)
    {
        $ok = $this->cart->remove($rowid);
        redirect('pembelian');
    }

    public function checkout()
    {
        if (!empty($_POST)) {
            $nofaktur = $this->input->post('nofaktur');
            $tanggal = $this->input->post('tanggal');
            $supplier = $this->input->post('supplier');
            $admin = $this->user['id'];
            $cart = $this->cart->contents();
            $total = 0;

            foreach ($cart as $result) {
                $total += $result['subtotal'];
            }

            $a_beli = [
                'nofaktur' => $nofaktur,
                'tanggal_beli' => $tanggal,
                'idsupplier' => $supplier,
                'total' => $total,
                'admin' => $admin
            ];


            $this->pembelian->beginTrans();
            $this->pembelian->insert($a_beli);

            foreach ($cart as $val) {
                $a_detail = [
                    'idpembelian' => $this->pembelian->getAI() - 1,
                    'idbarang' => $val['id'],
                    'harga_beli' => $val['price'],
                    'jumlah_beli' => $val['qty'],
                    'total_beli' => $val['subtotal']
                ];

                $a_update = [
                    'harga_pokok' => $val['price'],
                    'harga_jual' => $val['options']['harga_jual'],
                    'stok' => getBarang()[$val['id']]['stok'] + $val['qty']
                ];


                $this->belidetail->insert($a_detail);
                $this->barang->update($a_update, $val['id']);
            }

            $ok = $this->pembelian->statusTrans();
            $this->pembelian->commitTrans($ok);


            if ($ok) {
                $this->cart->destroy();
                setMessage('Berhasil menyimpan data pembelian', 'success');
            } else {
                setMessage('Gagal menyimpan data', 'danger');
            }

            redirect('pembelian');
        }
    }

    public function riwayat()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Riwayat Pembelian', '#');
        $data['title'] = 'Riwayat Pembelian';
        $data['profile'] = 'Pembelian';
        $data['user'] = $this->user;
        $idusaha = empty($this->session->userdata('idusaha')) ? null : $this->session->userdata('idusaha');
        $data['riwayat'] = $this->pembelian->getPembelian($idusaha)->result_array();
        $this->template->load('template', 'pembelian/riwayat', $data);
    }
}
