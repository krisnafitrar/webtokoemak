<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_kategori', 'kategori');
        $this->load->model('M_role', 'role');

        //set default
        $this->title = 'Data Kategori';
        $this->menu = 'kategori';
        $this->parent = '';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_data = [
            '1' => 'Aktif',
            '0' => 'Non-Aktif'
        ];

        $a_role = $this->role->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'kategori', 'label' => 'Kategori Produk'];
        $a_kolom[] = ['kolom' => 'idrole', 'label' => 'For Role', 'type' => 'S', 'option' => $a_role];
        $a_kolom[] = ['kolom' => 'icon', 'label' => 'Icon', 'is_tampil' => false, 'type' => 'F', 'path' => './assets/img/kategori/', 'file_type' => 'jpg|png|jpeg'];
        $a_kolom[] = ['kolom' => 'is_aktif', 'label' => 'Status', 'type' => 'S', 'option' => $a_data];

        $this->a_kolom = $a_kolom;
    }
}
