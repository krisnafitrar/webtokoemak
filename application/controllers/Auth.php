<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_user', 'users');
    }

    public function index()
    {
        if ($this->session->userdata('username')) {
            redirect('home');
        }

        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        if ($this->form_validation->run() == false) {
            $data['title'] = "Login Page";
            $this->template->load('template_auth', 'auth/login', $data);
        } else {
            //validasinya success
            $this->_login();
            // $this->_sendEmail();
        }
    }

    private function _login()
    {
        if ($this->session->userdata('username')) {
            redirect('user');
        }

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->users->getBy(['username' => $username])->row_array();

        //jika usernya ada
        if ($user) {
            //jika usernya aktif
            if ($user['is_aktif'] == 1) {
                //cek password
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'username' => $user['username'],
                        'role_id' => $user['role_id'],
                        'email' => $user['email'],
                        'idusaha' => $user['idusaha']
                    ];

                    $this->session->set_userdata($data);
                    if ($this->session->userdata['role_id'] < 3) {
                        redirect('home');
                    } else {
                        redirect('user');
                    }
                } else {
                    //password salah
                    setMessage('Password salah !', 'danger');
                    redirect('auth');
                }
            } else {
                //usernya tidak aktif
                setMessage('Akun anda telah di non-aktifkan', 'danger');
                redirect('auth');
            }
        } else {
            //usernya tidak ada
            setMessage('User tidak terdaftar', 'danger');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');
        $this->session->unset_userdata('idusaha');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil logout
            </div>');
        redirect('auth');
    }

    public function blocked()
    {
        $this->load->view('auth/blocked');
    }
}
