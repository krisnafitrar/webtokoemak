<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Supplier extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Data Supplier';
        $this->menu = 'supplier';
        $this->parent = '';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'idsupplier', 'label' => 'ID Supplier'];
        $a_kolom[] = ['kolom' => 'namasupplier', 'label' => 'Supplier'];
        $a_kolom[] = ['kolom' => 'notelp', 'label' => 'No Telp'];
        $a_kolom[] = ['kolom' => 'alamat', 'label' => 'Alamat', 'type' => 'A'];
        $a_kolom[] = ['kolom' => 'info_tambahan', 'label' => 'Info Tambahan', 'type' => 'A', 'is_null' => true, 'is_tampil' => false];

        $this->a_kolom = $a_kolom;
    }
}
