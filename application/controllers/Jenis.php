<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_kategori', 'kategori');

        //set default
        $this->title = 'Jenis Barang';
        $this->menu = 'jenis';
        $this->parent = '';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_data = $this->kategori->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'namajenis', 'label' => 'Jenis Produk'];
        $a_kolom[] = ['kolom' => 'idkategori', 'label' => 'Kategori', 'type' => 'S', 'option' => $a_data];

        $this->a_kolom = $a_kolom;
    }
}
